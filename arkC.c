#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "locallib.h"

int main()
{

    FILE *pont_arq; //Ponterio para arquivos
    FILE *file;     //Ponterio para arquivos
    
    struct ficha_aluno 
    {
        char nome[20];
        char matricula[10];
        float ap1;
        float ap2;
        float media;
    };

    struct ficha_aluno aluno;

    float notaMaior, notaMenor, mediaTotal;
    char  nomeMaior[20], nomeMenor[20], matriculaMaior[10], matriculaMenor[10];
    int num_alunos;

    notaMaior = 0;
    notaMenor = 11;
    mediaTotal = 0;
    num_alunos = 0;

    pont_arq = fopen("arquivo.txt", "r");
    file = fopen("notas.txt", "a");
    //printf("Digite o teste");

    //fscanf(pont_arq, "%i", &num_alunos);

    while(fscanf(pont_arq, "%s %s %f %f", aluno.nome, aluno.matricula, &aluno.ap1, &aluno.ap2) != EOF){

        printf(" %s %s %.2f %.2f\n", aluno.nome, aluno.matricula, aluno.ap1, aluno.ap2);

        aluno.media = (aluno.ap1+aluno.ap2)/2;

        if( aluno.media >= notaMaior ){
            notaMaior = aluno.media;
            strcpy(matriculaMaior, aluno.matricula);
            strcpy(nomeMaior, aluno.nome);

        } else {
            notaMenor = aluno.media;
            strcpy(matriculaMenor, aluno.matricula);
            strcpy(nomeMenor, aluno.nome);
        }

        fprintf(file, "%s %s %.2f %.2f precisa tirar na AP3 %.2f para passar\n", aluno.nome, aluno.matricula, aluno.ap1, aluno.ap2, AP3Minima(aluno.ap1, aluno.ap2));
        mediaTotal += aluno.media;
        num_alunos++;
    }
    /*
    for (int i = 0; i < num_alunos; i++)
     {
        fscanf(pont_arq, "%s %s %f %f", aluno.nome, aluno.matricula, &aluno.ap1, &aluno.ap2);
        printf(" %s %s %.2f %.2f\n", aluno.nome, aluno.matricula, aluno.ap1, aluno.ap2);

        aluno.media = (aluno.ap1+aluno.ap2)/2;

        if( aluno.media >= notaMaior ){
            notaMaior = aluno.media;
            strcpy(matriculaMaior, aluno.matricula);
            strcpy(nomeMaior, aluno.nome);

        } else {
            notaMenor = aluno.media;
            strcpy(matriculaMenor, aluno.matricula);
            strcpy(nomeMenor, aluno.nome);
        }

        fprintf(file, "%s %s %.2f %.2f precisa tirar na AP3 %.2f para passar\n", aluno.nome, aluno.matricula, aluno.ap1, aluno.ap2, AP3Minima(aluno.ap1, aluno.ap2));
        mediaTotal += aluno.media;
     }
    */
    fprintf(file, "\n\n O aluno da matricula %s com a maior nota é %s com %.2f de média!\n", matriculaMaior, nomeMaior, notaMaior);
    fprintf(file, " O aluno da matricula %s com a menor nota é %s com %.2f de média!\n", matriculaMenor, nomeMenor, notaMenor);
    fprintf(file,"E a média total da sala foi de %.2f", mediaTotal/num_alunos);

    fclose(pont_arq);
    fclose(file);


    return 0;
}
