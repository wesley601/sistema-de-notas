#ifndef LOCALLIB_H_INCLUDED
#define LOCALLIB_H_INCLUDED

float AP3Minima(float AP1, float AP2);

#endif

float AP3Minima(float AP1, float AP2){
    float media;
    media = (AP1 + AP2)*3;
    if( media >= 50){
        return 0;
    } else {
        return (50 - media)/10;
    }
}